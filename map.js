function getColor(val) {
  var col;
  if (val <= 0) { col = '#3182bd' } else
  if (val <= 1.809) { col = '#fee5d9' } else
  if (val <= 3.342) { col = '#fcae91' } else
  if (val <= 5.208) { col = '#fb6a4a' } else
  if (val <= 8.372) { col = '#de2d26' } else
  if (val <= 21) { col = '#a50f15' } else
    {col = "white"}

  var style = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: "lightgray",
      width: 0.7
    }),
    fill: new ol.style.Fill({
      color: col
    })
  })
  return style;
};

function niceDate(date) {
  var s = String(date)
  return parseInt(s.slice(4)) + '. ' + parseInt(s.slice(0, 4))
}

function makeTooltip(evt) {
 document.getElementById('tooltip').innerHTML = '<div id="chart"></div>'

var data = {};
for (var key in evt) {
  if (key.startsWith('nezam_tbl_d')) {
    data[parseInt(key.replace('nezam_tbl_d', ''))] = evt[key]
  }
}

 Highcharts.chart('chart', {
    chart: {
      height: 200,
      type: 'column'
    },
    title: {
        text: null
    },
    subtitle: {
      align: 'left',
      useHTML: true,
      text: '<b>' + evt.ObcePolygony_NAZ_OBEC + '</b>, okres ' + evt.ObcePolygony_NAZ_LAU1 + ' (' + evt.ObcePolygony_POCET_OBYV + ' obyv.)<br>Podíl uchazečů o práci k ' + niceDate(Object.keys(data).slice(-1)[0]) + ': <b>' + Math.round(Object.values(data).slice(-1)[0] * 10)/10  + ' %</b>' 
    },
    xAxis: {
        categories: Object.keys(data).map(function(v) {return niceDate(v)}),
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Uchazeči o práci (%)'
        }
    },
    credits: { enabled: false },
    tooltip: { enabled: false },
    legend: { enabled: false },
    plotOptions: {
        column: {
          animation: false,
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'nezamestnanost',
        data: Object.values(data)

    }]
});
};

var tilegrid = ol.tilegrid.createXYZ({tileSize: 512, maxZoom: 11});

var background = new ol.layer.Tile({
  source: new ol.source.OSM({
    url: 'https://interaktivni.rozhlas.cz/tiles/ton_b1/{z}/{x}/{y}.png',
    attributions: [
      new ol.Attribution({ html: 'obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>, data <a target="_blank" href="https://www.czso.cz/csu/czso/uchazeci-o-zamestnani-dosazitelni-a-podil-nezamestnanych-osob-podle-obci">Generální ředitelství Úřadu práce České republiky prostřednictvím ČSÚ</a>'})
    ]
  })
})

var labels = new ol.layer.Tile({
  source: new ol.source.OSM({
    url: 'https://interaktivni.rozhlas.cz/tiles/ton_l1/{z}/{x}/{y}.png'
  })
})

var layer = new ol.layer.VectorTile({
  source: new ol.source.VectorTile({
    format: new ol.format.MVT(),
    tileGrid: tilegrid,
    tilePixelRatio: 8,
    url: './tiles/{z}/{y}/{x}.pbf'
  }),
  style: function(feature) {
    return getColor(feature.get('nezam_tbl_d201706'))
  }
});

var map = new ol.Map({
   interactions: ol.interaction.defaults({mouseWheelZoom:false}),
  target: 'map',
  view: new ol.View({
    center: ol.proj.transform([15.3350758, 49.7417517], 'EPSG:4326', 'EPSG:3857'), //Číhošť
    zoom: 8,
    minZoom:7,
    maxZoom:11
  })
});

map.addLayer(background);
map.addLayer(layer);
map.addLayer(labels);

//geocoder
var geocoder = new Geocoder('nominatim', {
  provider: 'photon',
  //key: '__some_key__',
  lang: 'cs-CZ', //en-US, fr-FR
  placeholder: 'např. Plasy',
  targetType: 'glass-button',
  limit: 3,
  keepOpen: false,
});
map.addControl(geocoder);

geocoder.on('addresschosen', function(evt){
  var feature = evt.feature,
      coord = evt.coordinate,
      address = evt.address;
  // some popup solution
  content.innerHTML = '<p>'+ address.formatted +'</p>';
  overlay.setPosition(coord);
});

map.on('pointermove', function(evt) {
  if (evt.dragging) {
    return;
  }
  var pixel = map.getEventPixel(evt.originalEvent);
  if (map.hasFeatureAtPixel(pixel)){
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      makeTooltip(feature.c);
    });
  } else {
    document.getElementById('tooltip').innerHTML = 'Najetím vyberte obec.'
  }
});

//mobil
map.on('singleclick', function(evt) {
  var pixel = map.getEventPixel(evt.originalEvent);
  if (map.hasFeatureAtPixel(pixel)){
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      makeTooltip(feature.c);
    });
  } else {
    document.getElementById('tooltip').innerHTML = 'Najetím vyberte obec.'
  }
});